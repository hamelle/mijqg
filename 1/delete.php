<?php
//creation the session
session_start();
//include the connection to the database
include 'dataconnect.php';
?>
<?php require 'Navadmin.php';?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Modify</title>
<?php include 'styling.php';?>
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <br><br>
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">ICD</a>
        </li>
        <li class="breadcrumb-item active">Events</li>
      </ol>
      <h1>Modify</h1>
      <hr>
      <!-- Icon Cards-->
      <div class="row">

<?php
$p = "SELECT u.user_id, u.f_name, u.l_name, e.e_id, e.title, e.date
FROM users u, events e, reservation r
WHERE u.user_id = r.user_id
AND r.e_id = e.e_id";

$d = $conne->query($p);
$get = $d->fetch_all();
?>
        <div class="col-xl-6 col-sm-8 mb-2">
          <div class="container">
            <div class="card card-register mx-auto mt-5">
              <div class="card-header">Update</div>
                <div class="card-body">
                  <form method="post" action="delete.php">
                  <div class="form-group">
                      <div class="form-row">
                        <div class="col-md-6">
                          <label for="event">Event</label>
                          <select id="event" name="event" class="form-control"  aria-describedby="nameHelp">
                            <option>---Select event---</option>
                              <?php foreach($get as $found):
                              echo "<option value='$found[3]'>$found[4] on $found[5]</option>";
                              endforeach; ?>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="form-row">
                        <div class="col-md-6">
                          <label for="event">Participant</label>
                          <select id="event" name="p" class="form-control"  aria-describedby="nameHelp">
                            <option>---Select Participant---</option>
                              <?php foreach($get as $found):
                              echo "<option value='$found[0]'>$found[1] $found[2]</option>";
                              endforeach; ?>
                          </select>
                        </div>
                      </div>
                    </div>
                                        
                    <input type="submit" name="del" class="btn btn-danger btn-block" value="Remove Participant">
                  </form>
                </div>
              </div>
            </div>
          </div>
      </div>
  </div>
  <?php
  if(isset($_POST['del'])){
  $GLOBALS['id'] = $_POST['p'];
  $GLOBALS['e'] = $_POST['event'];
  $GLOBALS['res'] = $found[5];

  $q = "DELETE FROM reservation WHERE user_id = $id and e_id= $e";
  $delete = $conne->query($q);

  if(!$delete){
    echo "<script>alert('Could not remove event')</script>" . $conne->error;
  } else {
    echo "<script>alert('OK! Participant delete')</script>";
    echo "<script>window.location.assign('admin.php')</script>";

}
}
  ?>


<a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
</a>
    
    <!-- Bootstrap core JavaScript-->
    <script src="special/jquery/jquery.min.js"></script>
    <script src="special/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="special/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
  </div>
</body>

</html>
