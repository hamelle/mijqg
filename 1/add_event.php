<?php
//opening the session
session_start();

//connecting the page to the server and the database
include 'dataconnect.php';

//insert admin navigation bar
require 'Navadmin.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Add event</title>
  <?php include 'styling.php';?>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <br><br>
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">ICD</a>
        </li>
        <li class="breadcrumb-item active">Conferences</li>
      </ol>
      <h1>Add Event</h1>
      <hr>
      <!-- Icon Cards-->
      <div class="row">

        <div class="col-xl-6 col-sm-8 mb-2">
          <div class="container">
            <div class="card card-register mx-auto mt-5">
              <div class="card-header">Add event</div>
                <div class="card-body">
          <form method="post" action="add_event.php">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label>Title</label>
                <input class="form-control" type="text" aria-describedby="nameHelp" placeholder="Enter title" name="title">
              </div>
              <div class="col-md-6">
                <label>Type</label>
                <input class="form-control" type="text" aria-describedby="nameHelp" placeholder="Category of the event" name="type">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Details</label>
            <textarea cols="40" rows="4" name="details"></textarea>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-4">
                <label> Date</label>
                <input class="form-control" type="date" name="date" placeholder="Date">
              </div>
              <div class="col-md-3">
                <label for="exampleConfirmPassword">time</label>
                <input class="form-control" type="time" placeholder="Enter time" name="time">
              </div>
              <div class="col-md-5">
                <label>Venue</label>
                <input class="form-control" type="text" name="venue" placeholder="Venue">
              </div>
            </div>
          </div>
          <input type="submit" class="btn btn-success btn-block" value="Add Event" name="add"/>
        </form>
        </div>
        </div>
        </div>
        </div>

<?php
$del = "SELECT e_id, title, date FROM events";
  $find = $conne->query($del);
  $found = $find->fetch_all();

?>        
        <div class="col-xl-6 col-sm-8 mb-2">
          <div class="container">
            <div class="card card-register mx-auto mt-5">
              <div class="card-header">Delete</div>
                <div class="card-body">
                  <form method="post" action="add_event.php">
                    <div class="form-group">
                      <div class="form-row">
                        <div class="col-md-6">
                          <label >Event</label>
                          <select name="event" class="form-control"  aria-describedby="nameHelp">
                            <option>---Select event---</option>
                              <?php foreach($found as $row):
                              echo "<option value='$row[0]'>'$row[1]'  on  '$row[2]'</option>";
                              endforeach; ?>
                          </select>
                        </div>
                      </div>
                    </div>                    
                    <input class="btn btn-danger" type="submit" name="del" value="Remove event">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        </div>



<script src="special/jquery/jquery.min.js"></script>
  <script src="special/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="special/jquery-easing/jquery.easing.min.js"></script>

<?php

if(isset($_POST['add'])){
    $title = $_POST['title'];
    $type = $_POST['type'];
    $details = $_POST['details'];
    $date = $_POST['date'];
    $time = $_POST['time'];
    $venue = $_POST['venue'];

$q = "INSERT INTO events(title, type, details, date, time, venue)
VALUES ('$title', '$type', '$details', '$date', '$time',' $venue')"; 

$insert = $conne->query($q);

  if(!$insert){
    echo "'failed to add $title";
  } else {
    echo "<script>alert('OK! $title was added')</script>";
        echo "<script>window.location.assign('add_event.php')</script>";
  }
}

if(isset($_POST['del'])){
  $GLOBALS['id'] = $_POST['event'];
  $q = "DELETE FROM events WHERE e_id = $id";
  $delete = $conne->query($q);

  if(!$delete){
    echo "<script>alert('Could not remove event')</script>" . $conne->error;
  } else {
    echo "<script>alert('OK! event deleted')</script>";
    echo "<script>window.location.assign('add_event.php')</script>";

}
}

  ?>
</body>

</html>
