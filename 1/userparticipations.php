<?php
include "dataconnect.php";
?>
<?php require 'Navadmin.php';?>

<!DOCTYPE html>
<html lang="en">

<head>
<title>Admin</title>
  <?php include 'styling.php';?>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<?php

//query to display the parties on the page

  $sql = "SELECT count(user_id), e.title, e.date
  FROM reservation r, events e 
  WHERE r.e_id = e.e_id
  GROUP BY title";
  $result = $conne->query($sql);
  $find = $result->fetch_all();

$sql2 = "SELECT u.f_name, u.l_name, e.title, e.date
  FROM users u, reservation r, events e
  WHERE r.e_id = e.e_id
  AND u.user_id = r.user_id
  ORDER BY title";

  $u = $conne->query($sql2);
  $get = $u->fetch_all();
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">ICD</a>
        </li>
        <li class="breadcrumb-item active">Numbers</li>
      </ol>
<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Participants</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Participants</th>
                  <th>Events</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                   <th>Participants</th>
                  <th>Event</th>
                  <th>Date</th>
              </tfoot>
              <tbody>
                 <?php foreach($find as $found): ?>
                  <tr>
                  <?php foreach($found as $party): ?>
                   <td><?php echo $party ?></td>
                   <?php endforeach; ?>
                   </tr>
                    <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Events and parties</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Firstname</th>
                  <th>Lastname</th>
                  <th>Events</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                <th>Firstname</th>
                  <th>Lastname</th>
                  <th>Event</th>
                   <th>Date</th>
              </tfoot>
              <tbody>
                 <?php foreach($get as $user): ?>
                  <tr>
                  <?php foreach($user as $coming): ?>
                   <td><?php echo $coming ?></td>
                   <?php endforeach; ?>
                   </tr>
                    <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>    


    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
  <script src="special/jquery/jquery.min.js"></script>
    <script src="special/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="special/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="special/datatables/jquery.dataTables.js"></script>
    <script src="special/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>

 </div>
</body>
</html>