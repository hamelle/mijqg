<?php require 'Navuser.php';?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Reservation</title>
<?php include 'styling.php';?>
</head>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <br><br>
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">ICD</a>
        </li>
        <li class="breadcrumb-item active">Events</li>
      </ol>
      <h1>Conferences/ Academic conferences/ Professional</h1>
      <hr>
      <!-- Icon Cards-->
      <div class="row">

        <div class="col-xl-6 col-sm-8 mb-2">
          <div class="container">
            <div class="card card-register mx-auto mt-5">
              <div class="card-header">Reservation</div>
                <div class="card-body">
                  <form>
                    <div class="form-group">
                      <div class="form-row">
                        <div class="col-md-6">
                          <label for="event">Event</label>
                          <select id="event" name="event" class="form-control"  aria-describedby="nameHelp">
                            <option>---Select event---</option>
                              <?php foreach($find as $found):
                              echo "<option value='$found[0]'>$found[1]</option>";
                              endforeach; ?>
                          </select>
                        </div>
                      </div>
                    </div>                    
                    <input type="submit" name="Cancel" class="btn btn-danger btn-block" value="Cancel Participation" />
                  </form>
                </div>
              </div>
            </div>
          </div>  
        </div>
    </div>
  </div>



<a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
</a>
    
    <!-- Bootstrap core JavaScript-->
    <script src="special/jquery/jquery.min.js"></script>
    <script src="special/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="special/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
  </div>
</body>

</html>
