<?php
//opening the session
session_start();

//connecting the page to the server and the database
include 'dataconnect.php';

//insert admin navigation bar
require 'Navadmin.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Modify</title>
<?php include 'styling.php';?>
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <br><br>
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">ICD</a>
        </li>
        <li class="breadcrumb-item active">Events</li>
      </ol>
      <h1>Modify</h1>
      <hr>
      <!-- Icon Cards-->
      <div class="row">
        <div class="col-xl-6 col-sm-8 mb-2">
          <div class="container">
            <div class="card card-register mx-auto mt-5">
              <div class="card-header">Update</div>
                <div class="card-body">

<?php
$up = "SELECT * FROM events";
  $find = $conne->query($up);
  $found = $find->fetch_assoc();
  $get = $find->fetch_all();
?>   
          <form method="post" action="modify.php">
          <div class="form-group">
                      <div class="form-row">
                        <div class="col-md-6">
                          <label >Event to update</label>
                          <select name="event" class="form-control"  aria-describedby="nameHelp">
                            <option>---Select event---</option>
                              <?php foreach($get as $row):
                              echo "<option value='$row[0]'>$row[1]</option>";
                              endforeach; ?>
                          </select>
                        </div>
                      </div>
                    </div>
        <input type="submit" class="btn btn-primary btn-block" name="sel" value="select">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label>Title</label>
                <input class="form-control" type="text" placeholder="Title" name="title" value="<?php if(isset($_POST['sel'])) {echo $found['title']; }?>">
              </div>
              <div class="col-md-6">
                <label >Type</label>
                <input class="form-control" type="text" placeholder="type" name="type" value="<?php if(isset($_POST['sel'])) {echo $found['type']; }?>">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Details</label>
            <textarea cols="50" rows="5" name="details"><?php if(isset($_POST['sel'])) {echo $found['Details']; }?></textarea>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label> Date</label>
                <input class="form-control" type="date" name="date" placeholder="date" value="<?php if(isset($_POST['sel'])) {echo $found['Date']; }?>">
              </div>
              <div class="col-md-6">
                <label>time</label>
                <input class="form-control" type="text" placeholder="Time" name="time" value="<?php if(isset($_POST['sel'])) {echo $found['Time']; }?>">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Venue</label>
            <input type="text" name="venue" placeholder="venue" class="form-control" value="<?php if(isset($_POST['sel'])) {echo $found['Venue']; }?>">
          </div>
             <input type="submit" class="btn btn-success btn-block" value="Update" name="upd"/>

        </form>
         </div>
              </div>
            </div>
          </div>
    </div>
  </div>

<?php

if(isset($_POST['upd'])){
  $GLOBALS['id'] = $found['e_id'];;
    $title = $_POST['title'];
    $type = $_POST['type'];
    $details = $_POST['details'];
    $date = $_POST['date'];
    $time = $_POST['time'];
    $venue = $_POST['venue'];


$u = "UPDATE events SET title = '$title',
type = '$type', Details = '$details', Date = '$date', Time = '$time', venue = '$venue'
WHERE e_id = $id";

$update = $conne->query($u);

  if(!$update){
    echo "'failed to update $title".$conne->error;
  } else {
    echo "<script>alert('OK! $title was update')</script>";
        echo "<script>window.location.assign('modify.php')</script>";
  }
}
?>

<a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
</a>
    
    <!-- Bootstrap core JavaScript-->
    <script src="special/jquery/jquery.min.js"></script>
    <script src="special/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="special/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
  </div>
</body>

</html>
