<?php
session_start();
if(!$_SESSION['username'] && !$_SESSION['level'] == 'admin'){
  header('location: login.php');
}
?>

<?php
include "dataconnect.php";
?>
<?php require 'Navadmin.php';?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Admin</title>
  <?php include 'styling.php';?>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<?php

//query to display the parties on the page

  $sql = "SELECT * FROM events";
  $result = $conne->query($sql);
  $find = $result->fetch_all();
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">ICD</a>
        </li>
        <li class="breadcrumb-item active">Admin</li>
      </ol>
<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-fw fa-table"></i>EVENTS AND CONFERENCES ON SCHEDULE</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
              <th>ID</th>
              <th>title</th>
              <th>Type</th>
              <th>Details</th>
              <th>Date</th>
              <th>Time</th>
              <th>Venue</th>
              </tr>
              </thead>
              <tfoot>
                 <tr>
                <th>ID</th>
              <th>title</th>
              <th>Type</th>
              <th>Details</th>
              <th>Date</th>
              <th>Time</th>
              <th>Venue</th>
              </tr>
              </tfoot>
              <tbody>
                <?php foreach($find as $found): ?>
                  <tr>
                    <?php foreach ($found as $events): ?>
                    <td><?php echo $events ?></td>
                    <?php endforeach; ?>
                  </tr>
                    <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>



  <script src="special/jquery/jquery.min.js"></script>
    <script src="special/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="special/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="special/datatables/jquery.dataTables.js"></script>
    <script src="special/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>



  </div>
</body>

</html>
