<?php
session_start();
if(!$_SESSION['username']){
  header('location: login.php');
}
include 'dataconnect.php';
?>
<?php
include 'Navuser.php'
?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>ICD Service</title>
<?php include 'styling.php';?>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active">My Home</li>
      </ol>
      <?php

//query to display the parties on the page

  $sql = "SELECT * FROM events";
  $result = $conne->query($sql);
  $find = $result->fetch_all();
?>
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>EVENTS AND CONFERENCES ON SCHEDULE</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
              <th>ID</th>
              <th>title</th>
              <th>Type</th>
              <th>Details</th>
              <th>Date</th>
              <th>Time</th>
              <th>Venue</th>
              </tr>
              </thead>
              <tfoot>
                 <tr>
                <th>ID</th>
              <th>title</th>
              <th>Type</th>
              <th>Details</th>
              <th>Date</th>
              <th>Time</th>
              <th>Venue</th>
              </tr>
              </tfoot>
              <tbody>
                <?php foreach($find as $found): ?>
                  <tr>
                    <?php foreach ($found as $events): ?>
                    <td><?php echo $events ?></td>
                    <?php endforeach; ?>
                  </tr>
                    <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


		</div>
	</div>

   <div class="card-footer small text-muted">
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
        <!-- Bootstrap core JavaScript-->
    <script src="special/jquery/jquery.min.js"></script>
    <script src="special/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="special/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="special/chart.js/Chart.min.js"></script>
    <script src="special/datatables/jquery.dataTables.js"></script>
    <script src="special/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
    <script src="js/sb-admin-charts.min.js"></script>
	</div>
</body>

</html>
