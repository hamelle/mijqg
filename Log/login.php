<?php
//creation the session
session_start();
//include the connection to the database
include 'dataconnect.php';
?>

<?php // Run this script when the user clicks the submit button.
if(isset($_POST['submit'])){


  // Function to clean the variables
  function check($userinput){
    $userinput = trim($userinput);
    $userinput = stripslashes($userinput);
    $userinput = htmlspecialchars($userinput);
    return $userinput;
  }


  $username=check($_POST['user']);
  $password=(check($_POST['pass']));

  // Select user details from the database and compare it to what the user has inputed.
  $sql = "SELECT * FROM users WHERE Username = '$username'";
   $row = $conne->query($sql);
  $login = $row->fetch_assoc();


$_SESSION['user'] = $login['User_id'];
  $_SESSION['firstname'] = $login['f_name'];
  $_SESSION['lastname'] = $login['l_name'];
  $_SESSION['email'] = $login['Email'];
  $_SESSION['username'] = $login['Username'];
  $_SESSION['level'] = $login['Level'];
  // Set session variables
    if(empty($username) || empty($password)){
    echo "Wrong input";
  }
    //if user input match in the database
  elseif ($username == $login['Username'] && $password == $login['Password'] && $login['Level'] == 'user'){
    header('location: user.php');
  }
    //when the admin Rajinda logs in
  elseif ($username == $login['Username'] && $password == $login['Password'] && $login['Level'] == 'admin'){
    header('location: admin.php');
  }
    //when only either username or password value matches in the database
  elseif($username != $login['Username'] || $password != $login['Password']){
    $wrong = "<p class='error'>This user does not exist </p>";
     echo $wrong;
        echo $conne->error;
  }
}
// Close connection
 $conne->close();
 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Login | MIJQG Data</title>
    <?php include 'styling.php';?>
  </head>
  <body class="bg-dark">
    <div class="container">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login</div>
          <div class="card-body">
            <form method = "POST" action="login.php">
              <div class="form-group">
                <label>Username</label>
                <input class="form-control" type="text" placeholder="Enter username" name="user">
              </div>
              <div class="form-group">
                <label>Password</label>
                <input class="form-control" type="password" name="pass">
              </div>
          </div>
          <input type= "submit" class="btn btn-primary btn-block" name="submit" value="Login">
          <div class="text-center">
            <a class="d-block small mt-3" href="forgot-password.php">Forgot password?</a>
            <a class="d-block small mt-1" href="register.php">Register</a>
          </div>
        </form>

      </div>
    </div>
  </div>
   <script src="special/jquery/jquery.min.js"></script>
  <script src=specialr/bootstrap/js/bootstrap.bundle.min.js"></script>
   <script srcspecialor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
