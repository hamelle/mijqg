<!DOCTYPE html>
<html lang="en">

<head>
<title>Forgot password</title>
 <?php include 'styling.php';?>
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Reset Password</div>
      <div class="card-body">
        <div class="text-center mt-4 mb-5">
          <h4>Password forgotten?</h4>
          <p>Enter your Email and you will receive instructions to reset your password</p>
        </div>
        <form>
          <div class="form-group">
            <input class="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Enter email address">
          </div>
          <a class="btn btn-primary btn-block" href="">Reset Password</a>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="register.php">Create account</a>
          <a class="d-block small mt-1" href="login.php">Login</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="special/jquery/jquery.min.js"></script>
  <script src="special/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="special/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
