<?php
//opening the session
session_start();

//connecting the page to the server and the database
include 'dataconnect.php';
?>
<div class="php">
<?php

if(isset($_POST['submit'])){


 function check($value){
    $value = trim($value);
    $value = stripslashes($value);
    $value = htmlspecialchars($value);
    return $value;
  }

$firstname = check($_POST['firstname']);
$lastname = check($_POST['lastname']);
$email = check($_POST['email']);
$username = check($_POST['user']);
$password = (check($_POST['pass1']));
$pass_confirm = check($_POST['pass2']);
$phone = check($_POST['pho']);
  $ok = true;

  $sql = "INSERT INTO users (f_name, l_name, username, password, Email, Phone)
  VALUES ('$firstname', '$lastname', '$username', '$password', '$email', '$phone')";


    // check there's not allowed characters
    if (!preg_match("/^[a-zA-Z ]*$/",$firstname) or empty($firstname)) {
      $ok = false;
      echo "<p class='error'>Invalid input for first name</p>";
    }

    if (!preg_match("/^[a-zA-Z ]*$/",$lastname) or empty($lastname)) {
      $ok = false;
      $error_lastname = "Invalid input for last name";
      echo "<p class='error'>$error_lastname </p>";
    }

    if (!preg_match("/^[a-zA-Z ]*$/",$username) or empty($username)) {
      $ok = false;
      $error_username = "Invalid input for username";
      echo "<p class='error'>$error_username </p>";
    }
    if (!preg_match("/^[a-zA-Z0-9 ]*$/",$password) or empty($password)) {
      $ok = false;
      $error_pass = "Invalid input for password";
      echo "<p class='error'>$error_pass </p>";
    }
    if (!preg_match("/^[a-zA-Z0-9 ]*$/",$pass_confirm) or empty($pass_confirm)) {
      $ok = false;
      $error_passconfirm = "Invalid input for password confirmation";
      echo "<p class='error'>$error_passconfirm</p>";
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL) or empty($email)) {
      $ok = false;
      $error_email = "Invalid email format";
    echo "<p class='error'>$error_email</p>";
    }
    if($password != ($pass_confirm)){
      $ok = false;
      echo "<p class='error'>Passwords do not match</p>";
    }

#    //if all the validation is correct
     if($ok){
      if ($conne->query($sql)):
         echo "<p class='success'>user registered</p>";
          header ('location:login.php');
      else:
          echo "<p class='error'> Failed to register user</p>";
        endif;
    }
  }

?>

<!DOCTYPE html>
<html lang="en">

<head>
<title>Register</title>
  <?php include 'styling.php';?>
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Register an Account</div>
      <div class="card-body">
        <form method="Post" action="register.php">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label>First name</label>
                <input class="form-control"  type="text" name="firstname"  aria-describedby="nameHelp" placeholder="Enter first name">
              </div>
              <div class="col-md-6">
                <label >Last name</label>
                <input class="form-control"  type="text" name="lastname" aria-describedby="nameHelp" placeholder="Enter last name">
              </div>
              <div class="col-md-6">
                <label>Username</label>
                <input class="form-control"  type="text" name="user" aria-describedby="nameHelp" placeholder="Username">
              </div>
            </div>
          </div>
          <div class="form-group">

            <label >Email address</label>
            <input class="form-control" name="email" type="email" aria-describedby="emailHelp" placeholder="Enter email">
          </div>
           <div class="form-group">
            <label >Phone</label>
            <input class="form-control" name="pho" type="text" placeholder="Phone">
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label>Password</label>
                <input class="form-control" type="password" placeholder="Password" name="pass1">
              </div>
              <div class="col-md-6">
                <label for="exampleConfirmPassword">Confirm password</label>
                <input class="form-control" type="password" placeholder="Confirm password" name="pass2">
              </div>
            </div>
          </div>
          <input type="submit" name="submit" class="btn btn-primary btn-block" value="Register"/>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="login.php">Login</a>

        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="special/jquery/jquery.min.js"></script>
  <script src="special/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="special/jquery-easing/jquery.easing.min.js"></script>
</body>
</html>
