<?php
session_start();
if(!$_SESSION['username']){
  header('location: login.php');
}
include 'dataconnect.php';
?>
<?php require 'Navuser.php';?>
<!Doctype html>
<html>
<head>
  <title>Participations</title>
  <?php include 'styling.php';?>
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
 <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">ICD</a>
        </li>
        <li class="breadcrumb-item active">User</li>
      </ol>
      <?php
    $GLOBALS['activeuser'] = $_SESSION['user'];
      $sql = "SELECT e.title, e.date FROM events e, reservation r
      WHERE e.e_id = r.e_id
      AND r.user_id = $activeuser" ;

      $r = $conne->query($sql);
      $find = $r->fetch_all();
  ?>
  <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>EVENTS AND CONFERENCES ON SCHEDULE</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
              <th>Events</th>
              <th>Date</th>
               </tr>
              </thead>
              <tfoot>
                 <tr>
                <th>Events</th>
                <th>Date</th>
               </tr>
              </tfoot>
              <tbody>
              <?php foreach ($find as $found): ?>
                   <tr>
                    <?php foreach ($found as $events): ?>
                    <td><?php echo $events ?></td>
                    <?php endforeach; ?>
                  </tr>
                <?php endforeach ?>
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
        <div class="card-footer small text-muted">
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
        <!-- Bootstrap core JavaScript-->
    <script src="special/jquery/jquery.min.js"></script>
    <script src="special/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="special/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="special/chart.js/Chart.min.js"></script>
    <script src="special/datatables/jquery.dataTables.js"></script>
    <script src="special/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
    <script src="js/sb-admin-charts.min.js"></script>


</body>
</html>