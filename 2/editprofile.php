<?php
include 'dataconnect.php';
session_start();
if(!$_SESSION['username']){
  header('location: login.php');
}
?>

<?php require 'Navuser.php';?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Register</title>
  <?php include 'styling.php';?>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <br><br>
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">ICD</a>
        </li>
        <li class="breadcrumb-item active">Events</li>
      </ol>
      <h1>Edit profile</h1>
      <hr>
<?php
$GLOBALS['activeuser'] = $_SESSION['user'];
$up = "SELECT * FROM users WHERE User_id =$activeuser";
  $find = $conne->query($up);
  $found = $find->fetch_assoc();
?>
      <div class="row">
        <div class="col-xl-6 col-sm-8 mb-2">
          <div class="container">
            <div class="card card-register mx-auto mt-5">
              <div class="card-header">User Profile</div>
                <div class="card-body">
<form method="post" action="editprofile.php">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label>First name</label>
                <input class="form-control" type="text" value="<?php echo $found['f_name'];?>" name="f_name">
              </div>
              <div class="col-md-6">
                <label>Last name</label>
                <input class="form-control" type="text" value="<?php echo $found['l_name'];?>" name="l_name">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label >Email address</label>
            <input class="form-control"  type="email" value="<?php echo $found['Email'];?>" name="email">
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label >Password</label>
                <input class="form-control" type="password" placeholder="Password" name="pass">
              </div>
                </div>
          </div>
          <input type="submit" class="btn btn-success btn-block" value="Update" name="upd" />
        </form>

        <?php
        if(isset($_POST['upd'])){
          $GLOBALS['activeuser'] = $_SESSION['user'];
          $firtname = $_POST['f_name'];
          $lastname = $_POST['l_name'];
          $email = $_POST['email'];
          $pass = $_POST['pass'];

          $up = "UPDATE users SET f_name = '$firtname',
          l_name = '$lastname', Email = '$email', Password = '$pass'
          WHERE User_id = '$activeuser'";

          $done = $conne->query($up);

           if(!$done){
          echo "'failed to edit profile".$conne->error;
  } else {
        echo "<script>alert('OK! profile edited')</script>";
        echo "<script>window.location.assign('user.php')</script>";
      }
    }
            ?>


 <script src="special/jquery/jquery.min.js"></script>
  <script src="special/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="special/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
